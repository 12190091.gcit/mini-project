from audioop import ratecv

from flask import Flask,render_template,request
import pickle
model = pickle.load(open("testmodel.pkl","rb"))

app = Flask(__name__)

@app.route('/')
def index():
    
    return render_template('index.html')

@app.route('/pred',methods = ['GET'])
def pred():
    
    return render_template('Prediction.html')

@app.route('/pred',methods = ['POST'])
def prediction ():
    gender = request.form.get("gender")
    race = request.form.get("race")
    Parent_edu = request.form.get("parent_edu")
    Lunch = request.form.get("lunch")
    math = request.form.get("mathscore")
    read = request.form.get("readscore")
    write = request.form.get("writescore")
    arr = [[gender,race,Parent_edu,Lunch,math,read,write]]
    print(arr)
    pred = model.predict(arr)
    print(pred)
    return render_template('Prediction.html',predicted = pred[0])




if __name__ == "__main__":
    app.run(debug=True)