from http import server
import dash
import pandas as pd
import dash_core_components as dcc
import matplotlib.pyplot as plt
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go

df=pd.read_csv("StudentsPerformance.csv",)
df.rename(
    columns = ({'gender':'Gender', 'race/ethnicity':'Race', 'parental level of education':'Parental_Education', 'lunch':'Lunch', 'test preparation course':'Test_Preparation_Course', 'math score':'Math_Mark', 'reading score':'Reading_Mark', 'writing score':'Writing_Mark'}),
    inplace = True,
)

dfgroupA = df.groupby('Race')
dfgroupA = dfgroupA.get_group('group A')

matA = round(dfgroupA.Math_Mark.mean(),1)
readA = round(dfgroupA.Reading_Mark.mean(),1)
writeA = round(dfgroupA.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
marksA=[matA, readA, writeA]
subjectsA=['math_mark','reading_mark','writing_mark']
plt.bar(subjectsA,marksA)
for i in range(len(subjectsA)):
    plt.text(i,marksA[i],marksA[i],ha='center',va='bottom')

plt.title("Average Marks Of Group A",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)

dfgroupB = df.groupby('Race')
dfgroupB = dfgroupB.get_group('group B')

matB = round(dfgroupB.Math_Mark.mean(),1)
readB = round(dfgroupB.Reading_Mark.mean(),1)
writeB = round(dfgroupB.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
marksB=[matB, readB, writeB]
subjectsB=['math_mark','reading_mark','writing_mark']
plt.bar(subjectsB,marksB)
for i in range(len(subjectsB)):
    plt.text(i,marksB[i],marksB[i],ha='center',va='bottom')

plt.title("Average Marks Of Group B",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)

dfgroupC = df.groupby('Race')
dfgroupC = dfgroupC.get_group('group C')

matC = round(dfgroupC.Math_Mark.mean(),1)
readC = round(dfgroupC.Reading_Mark.mean(),1)
writeC = round(dfgroupC.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
marksC=[matC, readC, writeC]
subjectsC=['math_mark','reading_mark','writing_mark']
plt.bar(subjectsC,marksC)
for i in range(len(subjectsC)):
    plt.text(i,marksC[i],marksC[i],ha='center',va='bottom')

plt.title("Average Marks Of Group C",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)
dfgroupD = df.groupby('Race')
dfgroupD = dfgroupD.get_group('group D')

matD = round(dfgroupD.Math_Mark.mean(),1)
readD = round(dfgroupD.Reading_Mark.mean(),1)
writeD = round(dfgroupD.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
marksD=[matD, readD, writeD]
subjectsD=['math_mark','reading_mark','writing_mark']
plt.bar(subjectsD,marksD)
for i in range(len(subjectsD)):
    plt.text(i,marksD[i],marksD[i],ha='center',va='bottom')


plt.title("Average Marks Of Group D",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)
dfgroupE = df.groupby('Race')
dfgroupE = dfgroupE.get_group('group E')

matE = round(dfgroupE.Math_Mark.mean(),1)
readE = round(dfgroupE.Reading_Mark.mean(),1)
writeE = round(dfgroupE.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
marksE=[matE, readE, writeE]
subjectsE=['math_mark','reading_mark','writing_mark']
plt.bar(subjectsE,marksE)
for i in range(len(subjectsE)):
    plt.text(i,marksE[i],marksE[i],ha='center',va='bottom')

plt.title("Average Marks Of Group E",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)

dfgroupA = df.groupby('Parental_Education')
dfgroupA_bachelor = dfgroupA.get_group("bachelor's degree")
dfgroupA_college = dfgroupA.get_group("some college")
dfgroupA_master = dfgroupA.get_group("master's degree")
dfgroupA_associate = dfgroupA.get_group("associate's degree")
dfgroupA_high = dfgroupA.get_group("high school")
dfgroupA_some = dfgroupA.get_group("some high school")

bachelorA = round(dfgroupA_bachelor.Math_Mark.mean(),1)
CollegeA = round(dfgroupA_college.Math_Mark.mean(),1)
MasterA = round(dfgroupA_master.Math_Mark.mean(),1)
AssociateA = round(dfgroupA_associate.Math_Mark.mean(),1)
highA = round(dfgroupA_high.Math_Mark.mean(),1)
someA = round(dfgroupA_some.Math_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
parental_groupA = [bachelorA, CollegeA, MasterA, AssociateA, highA, someA]
subject=["Bachelor's Degree","College","Master's Degree", "Associate's Degree", "High School", "Some High School"]
plt.bar(subject,parental_groupA)
for i in range(len(subject)):
    plt.text(i,parental_groupA[i],parental_groupA[i],ha='right',fontsize=0,rotation=30)

plt.title("Average Math Marks Of Students As per Parental Degree",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)

dfgroupB = df.groupby('Parental_Education')
dfgroupB_bachelor = dfgroupB.get_group("bachelor's degree")
dfgroupB_college = dfgroupB.get_group("some college")
dfgroupB_master = dfgroupB.get_group("master's degree")
dfgroupB_associate = dfgroupB.get_group("associate's degree")
dfgroupB_high = dfgroupB.get_group("high school")
dfgroupB_some = dfgroupB.get_group("some high school")

bachelorB = round(dfgroupB_bachelor.Reading_Mark.mean(),1)
CollegeB = round(dfgroupB_college.Reading_Mark.mean(),1)
MasterB = round(dfgroupB_master.Reading_Mark.mean(),1)
AssociateB = round(dfgroupB_associate.Reading_Mark.mean(),1)
highB = round(dfgroupB_high.Reading_Mark.mean(),1)
someB = round(dfgroupB_some.Reading_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
parental_groupB = [bachelorB, CollegeB, MasterB, AssociateB, highB, someB]
subject=["Bachelor's Degree","College","Master's Degree", "Associate's Degree", "High School", "Some High School"]
plt.bar(subject,parental_groupB)
for i in range(len(subject)):
    plt.text(i,parental_groupB[i],parental_groupB[i],ha='right',fontsize=0,rotation=30)

plt.title("Average Reading Marks Of Students As per Parental Degree",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)
dfgroupC = df.groupby('Parental_Education')
dfgroupC_bachelor = dfgroupC.get_group("bachelor's degree")
dfgroupC_college = dfgroupC.get_group("some college")
dfgroupC_master = dfgroupC.get_group("master's degree")
dfgroupC_associate = dfgroupC.get_group("associate's degree")
dfgroupC_high = dfgroupC.get_group("high school")
dfgroupC_some = dfgroupC.get_group("some high school")

bachelorC = round(dfgroupC_bachelor.Writing_Mark.mean(),1)
CollegeC = round(dfgroupC_college.Writing_Mark.mean(),1)
MasterC = round(dfgroupC_master.Writing_Mark.mean(),1)
AssociateC = round(dfgroupC_associate.Writing_Mark.mean(),1)
highC = round(dfgroupC_high.Writing_Mark.mean(),1)
someC = round(dfgroupC_some.Writing_Mark.mean(),1)
plt.figure(figsize=(12,5), )
plt.subplots_adjust(hspace=0.5, wspace=0.25)
parental_groupC = [bachelorC, CollegeC, MasterC, AssociateC, highC, someC]
subject=["Bachelor's Degree","College","Master's Degree", "Associate's Degree", "High School", "Some High School"]
plt.bar(subject,parental_groupC)
for i in range(len(subject)):
    plt.text(i,parental_groupC[i],parental_groupC[i],ha='right',fontsize=0,rotation=30)

plt.title("Average Writing Marks Of Students As per Parental Degree",fontsize=18,y=1.05)
plt.xlabel('Subjects',fontsize=18)
plt.ylabel('Marks Obtained',fontsize=18)
##Group A
Test_Course_A_None_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group A') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_A_None_M = (Test_Course_A_None_M['Gender'] == 'male').count()
Total_Test_Course_A_None_M

Test_Course_A_Complete_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group A') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_A_Complete_M = (Test_Course_A_Complete_M['Gender'] == 'male').count()
Total_Test_Course_A_Complete_M

Test_Course_A_None_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group A') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_A_None_F = (Test_Course_A_None_F['Gender'] == 'female').count()
Total_Test_Course_A_None_F

Test_Course_A_Complete_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group A') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_A_Complete_F = (Test_Course_A_Complete_F['Gender'] == 'female').count()
Total_Test_Course_A_Complete_F

## Group B
Test_Course_B_None_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group B') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_B_None_M = (Test_Course_B_None_M['Gender'] == 'male').count()
Total_Test_Course_B_None_M

Test_Course_B_Complete_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group B') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_B_Complete_M = (Test_Course_B_Complete_M['Gender'] == 'male').count()
Total_Test_Course_B_Complete_M

Test_Course_B_None_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group B') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_B_None_F = (Test_Course_B_None_F['Gender'] == 'female').count()
Total_Test_Course_B_None_F

Test_Course_B_Complete_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group B') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_B_Complete_F = (Test_Course_B_Complete_F['Gender'] == 'female').count()
Total_Test_Course_B_Complete_F
## Group C
Test_Course_C_None_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group C') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_C_None_M = (Test_Course_C_None_M['Gender'] == 'male').count()
Total_Test_Course_C_None_M

Test_Course_C_Complete_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group C') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_C_Complete_M = (Test_Course_C_Complete_M['Gender'] == 'male').count()
Total_Test_Course_C_Complete_M

Test_Course_C_None_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group C') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_C_None_F = (Test_Course_C_None_F['Gender'] == 'female').count()
Total_Test_Course_C_None_F

Test_Course_C_Complete_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group C') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_C_Complete_F = (Test_Course_C_Complete_F['Gender'] == 'female').count()
Total_Test_Course_C_Complete_F
## Group D
Test_Course_D_None_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group D') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_D_None_M = (Test_Course_D_None_M['Gender'] == 'male').count()
Total_Test_Course_D_None_M

Test_Course_D_Complete_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group D') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_D_Complete_M = (Test_Course_D_Complete_M['Gender'] == 'male').count()
Total_Test_Course_D_Complete_M

Test_Course_D_None_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group D') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_D_None_F = (Test_Course_D_None_F['Gender'] == 'female').count()
Total_Test_Course_D_None_F

Test_Course_D_Complete_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group D') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_D_Complete_F = (Test_Course_D_Complete_F['Gender'] == 'female').count()
Total_Test_Course_D_Complete_F
## Group E
Test_Course_E_None_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group E') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_E_None_M = (Test_Course_E_None_M['Gender'] == 'male').count()
Total_Test_Course_E_None_M

Test_Course_E_Complete_M = df[(df['Gender'] == 'male') & (df["Race"] == 'group E') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_E_Complete_M = (Test_Course_E_Complete_M['Gender'] == 'male').count()
Total_Test_Course_E_Complete_M

Test_Course_E_None_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group E') & (df["Test_Preparation_Course"] == "none")]
Total_Test_Course_E_None_F = (Test_Course_E_None_F['Gender'] == 'female').count()
Total_Test_Course_E_None_F

Test_Course_E_Complete_F = df[(df['Gender'] == 'female') & (df["Race"] == 'group E') & (df["Test_Preparation_Course"] == "completed")]
Total_Test_Course_E_Complete_F = (Test_Course_E_Complete_F['Gender'] == 'female').count()
Total_Test_Course_E_Complete_F

## math mark
Test_Math_None = df[(df["Test_Preparation_Course"] == "none")]
Total_Test_None = Test_Math_None.Math_Mark.mean()
Total_Test_None

Test_Math_completed = df[(df["Test_Preparation_Course"] == "completed")]
Total_Test_completed = Test_Math_completed.Math_Mark.mean()
Total_Test_completed


## Reading Mark
Test_Read_None = df[(df["Test_Preparation_Course"] == "none")]
Total_Read_None = Test_Read_None.Reading_Mark.mean()
Total_Read_None

Test_Read_Completed = df[(df["Test_Preparation_Course"] == "completed")]
Total_Read_Completed = Test_Read_Completed.Reading_Mark.mean()
Total_Read_Completed

## Writing Mark
Test_Write_None = df[(df["Test_Preparation_Course"] == "none")]
Total_Write_None = Test_Write_None.Writing_Mark.mean()
Total_Write_None

Test_Write_Completed = df[(df["Test_Preparation_Course"] == "completed")]
Total_Write_Completed = Test_Write_Completed.Writing_Mark.mean()
Total_Write_Completed
colors = ['red', 'blue']


app = dash.Dash(__name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}])
server = app.server
tabs_styles = {
    'height': '44px',
    'align-items': 'center'
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px',
    'fontWeight': 'bold',
    'border-radius': '15px',
    'background-color': '#F2F2F2',
    'box-shadow': '4px 4px 4px 4px lightgrey',

}

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px',
    'border-radius': '15px',
}

app.layout = html.Div(style = {'backgroundColor' : '#F5F5DC'
},children = [
              html.H1('STUDENT PERFORMANCE ANALYSIS', style = {'textAlign':'center','color': 'black'}),
                dcc.Tabs(id = "tabs-styled-with-inline", value = 'tab-1', children = [
                dcc.Tab(label = 'Subject Wise Marks', value = 'tab-1', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Test Prepation Course', value = 'tab-2', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Average Marks', value = 'tab-3', style = tab_style, selected_style = tab_selected_style),
                dcc.Tab(label = 'Average Marks per Test Prepation Course', value = 'tab-4', style = tab_style, selected_style = tab_selected_style),
            ], style = tabs_styles),
            html.Div(id = 'tabs-content-inline')
])

@app.callback(Output('tabs-content-inline', 'children'),
              Input('tabs-styled-with-inline', 'value'))
def render_content(tab):
    if tab == 'tab-1':
        return html.Div([
            html.Div([
                html.Div([
                    html.H3('Select Group', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_group',
                                 multi = False,
                                 clearable = True,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'group A',
                                 placeholder = 'Select Group',
                                 options = [{'label': 'Group A', 'value': 'group A'},
                                            {'label': 'Group B','value': 'group B'},
                                            {'label': 'Group C','value': 'group C'},
                                            {'label': 'Group D','value': 'group D'},
                                            {'label': 'Group E','value': 'group E'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'group_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    
    elif tab == 'tab-2':
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Group', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_race',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'group A',
                                 placeholder = 'Select Year',
                                 options = [{'label': 'Group A', 'value': 'group A'},
                                            {'label': 'Group B', 'value': 'group B'},
                                            {'label': 'Group C', 'value': 'group C'},
                                            {'label': 'Group D', 'value': 'group D'},
                                            {'label': 'Group E', 'value': 'group E'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),

            html.Div([
                html.Div([
                    html.P('Select Gender Type', className = 'fix_label', style = {'color': 'black'}),
                    dcc.RadioItems(id = 'gender_item',
                                   labelStyle = {"display": "inline-block"},
                                   options = [
                                       {'label': 'Male', 'value': 'Male'},
                                       {'label': 'Female', 'value': 'Female'}],
                                   value = 'Male',
                                   style = {'text-align': 'center', 'color': 'black'}, className = 'dcc_compon'),

                    dcc.Graph(id = 'gender_chart',
                              config = {'displayModeBar': 'hover'}),

                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),

            ], className = "row flex-display"),
        ])

    elif tab == 'tab-3':
         return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Group', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_group',
                                 multi = False,
                                 clearable = True,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'Math_Mark',
                                 placeholder = 'Select Subject',
                                 options = [{'label': 'Math Mark', 'value': 'Math_Mark'},
                                            {'label': 'Reading Mark','value': 'Reading_Mark'},
                                            {'label': 'Writing Mark','value': 'Writing_Mark'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'parent_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
        
        
    elif tab == 'tab-4':
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Year', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_year',
                                 multi = False,
                                 clearable = True,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'Math_Mark',
                                 placeholder = 'Select Subject',
                                 options = [{'label': 'Math', 'value': 'Math_Mark'},
                                            {'label': 'Reading','value': 'Reading_Mark'},
                                            {'label': 'Writing','value': 'Writing_Mark'}], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'average_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    
    
        
        
    
@app.callback(
Output('group_chart', 'figure'), 
[Input('select_group', 'value')]
)
#graph plot and styling
def update_graph(value):
    if value == 'group A':
        return {'data':[go.Bar(
                                x = subjectsA,
                                y = marksA
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Subject wise Marks of ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Subject</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    if value == 'group B':
        return {'data':[go.Bar(
                                x = subjectsB,
                                y = marksB
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Subject wise Marks of ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Subject</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    if value == 'group C':
        return {'data':[go.Bar(
                                x = subjectsC,
                                y = marksC
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Subject wise Marks of ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Subject</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    if value == 'group D':
        return {'data':[go.Bar(
                                x = subjectsD,
                                y = marksD
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Subject wise Marks of ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Subject</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    if value == 'group E':
        return {'data':[go.Bar(
                                x = subjectsE,
                                y = marksE
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Subject wise Marks of ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Subject</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }
    
@app.callback(Output('gender_chart', 'figure'),
              [Input('select_race', 'value')],
              [Input('gender_item', 'value')]
             )
def update_graph(select_race, gender_item):
    if select_race == 'group A':
        if gender_item == 'Male':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_A_None_M, Total_Test_Course_A_Complete_M],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        if gender_item == 'Female':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_A_None_F, Total_Test_Course_A_Complete_F],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        
    if select_race == 'group B':
        if gender_item == 'Male':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Completed', 'Not Completed'],
                                   values = [Total_Test_Course_B_None_M, Total_Test_Course_B_Complete_M],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        if gender_item == 'Female':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_B_None_F, Total_Test_Course_B_Complete_F],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        
    if select_race == 'group C':
        if gender_item == 'Male':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_C_None_M, Total_Test_Course_C_Complete_M],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        if gender_item == 'Female':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_C_None_F, Total_Test_Course_C_Complete_F],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        
    if select_race == 'group D':
        if gender_item == 'Male':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_D_None_M, Total_Test_Course_D_Complete_M],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        if gender_item == 'Female':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_D_None_F, Total_Test_Course_D_Complete_F],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            } 
        
    if select_race == 'group E':
        if gender_item == 'Male':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_E_None_M, Total_Test_Course_E_Complete_M],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        if gender_item == 'Female':
            color = ['green', 'orange']
            return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                                   values = [Total_Test_Course_E_None_F, Total_Test_Course_E_Complete_F],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Test Preparation Course Of ' + (select_race),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
@app.callback(
Output('parent_chart', 'figure'), 
[Input('select_group', 'value')]
)
#graph plot and styling
def update_graph(value):
    if value == 'Math_Mark':
        return {'data':[go.Bar(
                                x = subject,
                                y = parental_groupA
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Math Mark As Per Parental Education',
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Parental Education</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }    
    
    if value == 'Reading_Mark':
        return {'data':[go.Bar(
                                x = subject,
                                y = parental_groupB
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Reading Mark As Per Parental Education',
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Parental Education</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }    
    
    if value == 'Writing_Mark':
        return {'data':[go.Bar(
                                x = subject,
                                y = parental_groupC
                                ),
                             
                        ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Writing Mark As Per Parental Education',
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>Parental Education</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Marks</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        )    
               }  
    
@app.callback(
Output('average_chart', 'figure'), 
[Input('select_year', 'value')]
)
#graph plot and styling

def update_graph(value):
    if value == 'Math_Mark':
        return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                               values = [Total_Test_None, Total_Test_completed],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Average Math Mark As Per Test Preparation Course ',
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.14},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    if value == 'Reading_Mark':
        return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                               values = [Total_Read_None, Total_Read_Completed],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Average Reading Mark As Per Test Preparation Course ',
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.14},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
    
    if value == 'Writing_Mark':
        return {'data':[go.Pie(labels = ['Not Completed', 'Completed'],
                               values = [Total_Write_None, Total_Write_Completed],
                               marker = dict(colors = colors),
                               hoverinfo = 'label+value+percent',
                               textinfo = 'label+value',
                               textfont = dict(size = 11),
                               texttemplate = '%{label}: %{value} <br>(%{percent})',
                               textposition = 'auto',
                               hole = .7,
                               rotation = 100
                              ),
                       ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            hovermode = 'x',
                            title = {
                                'text': 'Average Writing Mark As Per Write Preparation Course ',
                                'y': 0.9,
                                'x': 0.5,
                                'xanchor': 'center',
                                'yanchor': 'top'},
                            titlefont = {
                                'color': 'black',
                                'size': 15},
                            legend = {
                                'orientation': 'h',
                                'bgcolor': '#F2F2F2',
                                'xanchor': 'center', 'x': 0.5, 'y': -0.14},
                            font = dict(
                                family = "sans-serif",
                                size = 12,
                                color = 'black')
                        ),                           
               }
if __name__ == '__main__':
    app.run_server()